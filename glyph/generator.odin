package glyph

import "core:log"
import ft "shared:freetype"
import fixed "core:math/fixed"
import "core:c"
import "core:mem"

Render_Mode :: enum u8 {
  NORMAL,
  LCD,
}


Glyph_Gen :: struct {
  render_mode: ft.Render_Mode,
  load_flags: i32,
  load_target: i32,
  bytes_per_pixel: u32,
  char_width: ft.F26Dot6,
  char_height: ft.F26Dot6,
  x_dpi: u32,
  y_dpi: u32,
}

glyph_gen_set_face_size :: proc(gen: ^Glyph_Gen, face: ft.Face) -> ft.Error {
  return ft.Set_Char_Size(face, gen.char_width, gen.char_height, gen.x_dpi, gen.y_dpi);
}

glyph_gen_init_point_size :: proc(gen: ^Glyph_Gen, render_mode: Render_Mode, point_height: f64, x_dpi: u32, y_dpi: u32) {
  f26d6: fixed.Fixed26_6;
  fixed.init_from_f64(&f26d6, point_height);
  frac_point_size: ft.F26Dot6 = ft.F26Dot6(f26d6.i);
  rm, flags := to_ft_render_mode(render_mode);
  gen.render_mode = rm;
  gen.load_flags = flags;
  gen.bytes_per_pixel = bytes_per_pixel(render_mode);
  gen.char_width = frac_point_size;
  gen.char_height = frac_point_size;
  gen.x_dpi = x_dpi;
  gen.y_dpi = y_dpi;
}

glyph_gen_init_pixel_size :: proc(gen: ^Glyph_Gen, render_mode: Render_Mode, pixel_height: u16, x_dpi: u32, y_dpi: u32) {
  dpi := y_dpi;
  if dpi == 0 do dpi = x_dpi;
  point_size := f64(pixel_height)/f64(dpi)*72.0;
  glyph_gen_init_point_size(gen, render_mode, point_size, x_dpi, y_dpi);
}

@private
to_ft_render_mode :: proc(render_mode: Render_Mode) -> (ft.Render_Mode, i32) {
  switch render_mode {
    case .NORMAL: return ft.Render_Mode.NORMAL, ft.LOAD_TARGET_NORMAL;
    case .LCD: return ft.Render_Mode.LCD, ft.LOAD_TARGET_LCD | ft.LOAD_COLOR;
  }
  return ft.Render_Mode.NORMAL, ft.LOAD_TARGET_NORMAL;
}

@private
bytes_per_pixel :: proc(render_mode: Render_Mode) -> u32 {
  switch render_mode {
    case .NORMAL: return 1;
    case .LCD: return 4;
  }
  return 1;
}

glyph_gen_get_max_pixel_dims :: proc(gen: ^Glyph_Gen, face: ft.Face, start_char: u32, end_char: u32) -> (max_width: u32, max_height: u32, ok: bool) {
  glyph_index: u32;
  current_char: c.ulong;
  for current_char = ft.Get_First_Char(face, &glyph_index); glyph_index != 0 && current_char < c.ulong(start_char); current_char = ft.Get_Next_Char(face, current_char, &glyph_index) {}

  for ;glyph_index != 0 && current_char < c.ulong(end_char); current_char = ft.Get_Next_Char(face, current_char, &glyph_index) {
    if err := ft.Load_Glyph(face, glyph_index, gen.load_flags); err != 0 {
      log.errorf("Failed to load Glyph: %v", err);
      return;
    }
    w := u32(face.glyph.metrics.width >> 6);
    if face.glyph.metrics.width % 64 > 0 {
      w += 1;
    }
    h := u32(face.glyph.metrics.height >> 6);
    if face.glyph.metrics.height % 64 > 0 {
      h += 1;
    }

    if w > max_width {
      max_width = w;
    }
    if h > max_height {
      max_height = h;
    }
  }
  ok = true;
  return;
}

glyph_gen_render_glyph :: proc(gen: ^Glyph_Gen, face: ft.Face, glyph_index: u32, dest: ^Glyph_Texture, x_pixel, y_pixel: u32) -> bool {
  if err := ft.Load_Glyph(face, glyph_index, gen.load_flags); err != 0 {
    log.errorf("Failed to load Glyph: %v", err);
    return false;
  }
  glyph_len := face.glyph.bitmap.width * face.glyph.bitmap.rows;
  glyph_bits := mem.slice_ptr(face.glyph.bitmap.buffer, int(glyph_len));
  row_end := face.glyph.bitmap.width;
  fixup := (dest.half_glyph_pixel_width - (row_end/2))*gen.bytes_per_pixel;
  baseIdx: u32 = x_pixel * gen.bytes_per_pixel + y_pixel * dest.row_bytes_width;
  yfixup : u32 = u32(i32(dest.ascender_pixels) - face.glyph.bitmap_top);
  for yi : u32 = 0; yi < face.glyph.bitmap.rows; yi += 1 {
    ydest := yi + yfixup;
    ds := baseIdx + fixup + (ydest * dest.row_bytes_width);
    ss := yi * face.glyph.bitmap.width;
    copy(dest.buffer[ds:], glyph_bits[ss:row_end]);
    row_end += face.glyph.bitmap.width;
  }
  return true;
}
