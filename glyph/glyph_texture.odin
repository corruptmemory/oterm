package glyph

Glyph_Texture :: struct {
  buffer: []byte,
  row_bytes_width: u32,
  pixel_width: u32,
  pixel_height: u32,
  glyph_pixel_width: u32,
  glyph_pixel_height: u32,
  half_glyph_pixel_width: u32,
  ascender_pixels: u32,
}


