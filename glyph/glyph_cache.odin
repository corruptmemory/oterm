package glyph

import conf "../build_config"
import rt "core:runtime"
import "core:math"
import "core:mem"

Glyph_Hash :: u64;

Gpu_Glyph_Index :: u32;

Glyph_Cache_Point :: struct {
  x, y: u32,
};

Glyph_Fill_State :: enum u8 {
  EMPTY,
  FILLED,
};

when conf.DEBUG_VALIDATE_LRU {
  Glyph_Entry :: struct {
    hash_value:          Glyph_Hash,
    next_with_same_hash: u32,
    next_lru:            u32,
    prev_lru:            u32,
    gpu_index:           Gpu_Glyph_Index,
    filled_state:        Glyph_Fill_State,
    dim_x:               u16,
    dim_y:               u16,
    ordering:            i32,
  };
} else {
  Glyph_Entry :: struct {
    hash_value:          Glyph_Hash,
    next_with_same_hash: u32,
    next_lru:            u32,
    prev_lru:            u32,
    gpu_index:           Gpu_Glyph_Index,
    filled_state:        Glyph_Fill_State,
    dim_x:               u16,
    dim_y:               u16,
  };
}

Glyph_Table_Stats :: struct {
  hit_count:     u32,
  miss_count:    u32,
  recycle_count: u32,
};

Glyph_State :: struct {
    id:           u32,
    gpu_index:    Gpu_Glyph_Index,
    filled_state: Glyph_Fill_State,
    dim_x:        u16,
    dim_y:        u16,
};

when conf.DEBUG_VALIDATE_LRU {
  Glyph_Table :: struct {
    stats:          Glyph_Table_Stats,
    hash_mask:      u32,
    hash_table:     []u32,
    entries:        []Glyph_Entry,
    last_lru_count: u32,
  };
} else {
  Glyph_Table :: struct {
    stats:       Glyph_Table_Stats,
    hash_mask:   u32,
    hash_table:  []u32,
    entries:     []Glyph_Entry,
  };
}

Glyph_Table_Params :: struct {
    hash_count:            u32,
    entry_count:           u32,
    reserved_tile_count:   u32,
    cache_tile_count_in_x: u32,
};

pack_glyph_cache_point :: #force_inline proc(x, y: u32) -> Gpu_Glyph_Index {
  return (y << 16) | x;
}

unpack_glyph_cache_point :: #force_inline proc(p: Gpu_Glyph_Index) -> Glyph_Cache_Point {
  return Glyph_Cache_Point {
    x = p & 0xffff,
    y = p >> 16,
  };
}

glyph_hashes_are_equal :: #force_inline proc(a, b: Glyph_Hash) -> bool {
  return a == b;
}

get_slot_pointer :: proc(table: ^Glyph_Table, run_hash: Glyph_Hash) -> ^u32 {
  hash_index := u32(run_hash & 0x00000000ffffffff);
  hash_slot := (hash_index & table.hash_mask);
  return &table.hash_table[hash_slot];
}

get_entry :: proc(table: ^Glyph_Table, index: u32) -> ^Glyph_Entry {
  result := &table.entries[index];
  return result;
}

get_sentinel :: proc(table: ^Glyph_Table) -> ^Glyph_Entry {
  return &table.entries[0];
}

get_and_clear_stats :: proc(table: ^Glyph_Table) -> Glyph_Table_Stats {
  result := table.stats;
  table.stats = Glyph_Table_Stats{};
  return result;
}

update_glyph_cache_entry :: proc(table: ^Glyph_Table, id: u32, new_state: Glyph_Fill_State, new_dim_x, new_dim_y: u16) {
  entry := get_entry(table, id);
  entry.filled_state = new_state;
  entry.dim_x = new_dim_x;
  entry.dim_y = new_dim_y;
}

when conf.DEBUG_VALIDATE_LRU {
  validate_lru :: proc(table: ^Glyph_Table, expected_count_change: int) {
    entry_count : u32 = 0;

    sentinel := get_sentinel(table);
    last_ordering := sentinel.ordering;
    for entry_index := sentinel.next_lru; entry_index != 0; {
      entry := get_entry(table, entry_index);
      assert(entry.ordering < last_ordering);
      last_ordering = entry.ordering;
      entry_index = entry.next_lru;
      entry_count += 1;
    }

    if (table->last_lru_count + expected_count_change) != entry_count {
      panic("Cache validation failed");
    }
    table.last_lru_count = entry_count;
  }
} else {
  validate_lru :: proc(table: ^Glyph_Table, expected_count_change: int) {}
}

recycle_lru :: proc(table: ^Glyph_Table) {
  sentinel := get_sentinel(table);

  assert(sentinel.prev_lru != 0);

  entry_index := sentinel.prev_lru;
  entry := get_entry(table, entry_index);
  prev := get_entry(table, entry.prev_lru);
  prev.next_lru = 0;
  sentinel.prev_lru = entry.prev_lru;
  validate_lru(table, -1);

  next_index: ^u32;
  for next_index = get_slot_pointer(table, entry.hash_value);
      next_index^ != entry_index;
      next_index = &get_entry(table, next_index^).next_with_same_hash {
    assert(next_index^ != 0);
  }

  assert(next_index^ == entry_index);
  // NOTE(jim): This value isn't used
  next_index^ = entry.next_with_same_hash;
  entry.next_with_same_hash = sentinel.next_with_same_hash;
  sentinel.next_with_same_hash = entry_index;

  // NOTE(casey): Clear the index count and state
  update_glyph_cache_entry(table, entry_index, .EMPTY, 0, 0);

  table.stats.recycle_count += 1;
}

pop_free_entry :: proc(table: ^Glyph_Table) -> u32 {
  sentinel := get_sentinel(table);

  if sentinel.next_with_same_hash == 0 {
    recycle_lru(table);
  }

  result := sentinel.next_with_same_hash;
  assert(result != 0);

  // NOTE(casey): Pop this unused entry off the sentinel's chain of unused entries
  entry := get_entry(table, result);
  sentinel.next_with_same_hash = entry.next_with_same_hash;
  entry.next_with_same_hash = 0;

  assert(entry != nil);
  assert(entry != sentinel);
  assert(entry.dim_x == 0);
  assert(entry.dim_y == 0);
  assert(entry.filled_state == .EMPTY);
  assert(entry.next_with_same_hash == 0);
  assert(entry == get_entry(table, result));

  return result;
}

find_glyph_entry_by_hash :: proc(table: ^Glyph_Table, run_hash: Glyph_Hash) -> Glyph_State {
  result : ^Glyph_Entry = nil;

  slot := get_slot_pointer(table, run_hash);
  entry_index: u32;
  for entry_index = slot^;entry_index != 0; {
    entry := get_entry(table, entry_index);
    if glyph_hashes_are_equal(entry.hash_value, run_hash) {
      result = entry;
      break;
    }
    entry_index = entry.next_with_same_hash;
  }

  if result != nil {
    assert(entry_index != 0);

    // NOTE(casey): An existing entry was found, remove it from the LRU
    prev := get_entry(table, result.prev_lru);
    next := get_entry(table, result.next_lru);
    
    prev.next_lru = result.next_lru;
    next.prev_lru = result.prev_lru;
    
    validate_lru(table, -1);

    table.stats.hit_count += 1;
  }
  else
  {
    // NOTE(casey): No existing entry was found, allocate a new one and link it into the hash chain

    entry_index = pop_free_entry(table);
    assert(entry_index != 0);

    result = get_entry(table, entry_index);
    assert(result.filled_state == .EMPTY);
    assert(result.next_with_same_hash == 0);
    assert(result.dim_x == 0);
    assert(result.dim_y == 0);
    
    result.next_with_same_hash = slot^;
    result.hash_value = run_hash;
    slot^ = entry_index;

    table.stats.miss_count += 1;
  }

  // NOTE(casey): Update the LRU doubly-linked list to ensure this entry is now "first"
  sentinel := get_sentinel(table);
  assert(result != sentinel);
  result.next_lru = sentinel.next_lru;
  result.prev_lru = 0;

  next_lru := get_entry(table, sentinel.next_lru);
  next_lru.prev_lru = entry_index;
  sentinel.next_lru = entry_index;

  when conf.DEBUG_VALIDATE_LRU {
    result.ordering = sentinel.ordering;
    sentinel.ordering += 1;
  }
  validate_lru(table, 1);

  return Glyph_State {
    id           = entry_index,
    dim_x        = result.dim_x,
    dim_y        = result.dim_y,
    gpu_index    = result.gpu_index,
    filled_state = result.filled_state,
  };
}

initialize_direct_glyph_table :: proc(params: Glyph_Table_Params, table: []Gpu_Glyph_Index, skip_zero_slot: bool) {
  assert(params.cache_tile_count_in_x >= 1);

  szs: u32;
  if skip_zero_slot do szs = 1;
  
  x: u32 = szs;
  y: u32;
  for entry_index : u32 = 0;
      entry_index < (params.reserved_tile_count - szs);
      entry_index +=1 {
    if x >= params.cache_tile_count_in_x {
      x = 0;
      y += 1;
    }

    table[entry_index] = pack_glyph_cache_point(x, y);

    x += 1;
  }
}

get_glyph_table_footprint :: proc(params: Glyph_Table_Params) -> u32 {
  hash_size := params.hash_count * size_of(u32);
  entry_size := params.entry_count * size_of(Glyph_Entry);
  return  u32(size_of(Glyph_Table) + hash_size + entry_size);
}

place_glyph_table_in_memory :: proc(params: Glyph_Table_Params, memory: rawptr) -> ^Glyph_Table {
  assert(params.hash_count >= 1);
  assert(params.entry_count >= 2);
  assert(math.is_power_of_two(int(params.hash_count)));
  assert(params.cache_tile_count_in_x >= 1);

  result: ^Glyph_Table = nil;

  if memory != nil {
    // NOTE(casey): Always put the glyph_entry array at the base of the memory, because the
    // compiler may generate aligned-SSE ops, which would crash if it was unaligned.
    entries := mem.slice_ptr(cast(^Glyph_Entry)(memory), int(params.entry_count));
    result = cast(^Glyph_Table)(mem.ptr_offset(raw_slice_data(entries), int(params.entry_count)));
    result.hash_table = mem.slice_ptr(cast(^u32)(mem.ptr_offset(result,1)), int(params.hash_count));
    result.entries = entries;

    result.hash_mask = params.hash_count - 1;

    rt.memset(raw_slice_data(result.hash_table), 0, len(result.hash_table)*size_of(result.hash_table[0]));

    starting_tile := params.reserved_tile_count;

    sentinel := get_sentinel(result);
    x := starting_tile % params.cache_tile_count_in_x;
    y := starting_tile / params.cache_tile_count_in_x;
    for entry_index : u32 = 0;
        entry_index < params.entry_count;
        entry_index += 1 {
      if x >= params.cache_tile_count_in_x {
        x = 0;
        y += 1;
      }

      entry := get_entry(result, entry_index);
      if (entry_index+1) < params.entry_count {
        entry.next_with_same_hash = entry_index + 1;
      } else {
        entry.next_with_same_hash = 0;
      }
      entry.gpu_index = pack_glyph_cache_point(x, y);

      entry.filled_state = .EMPTY;
      entry.dim_x = 0;
      entry.dim_y = 0;
      
      x += 1;
    }

    get_and_clear_stats(result);
  }

  return result;
}
