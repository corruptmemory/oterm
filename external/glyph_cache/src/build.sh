#!/usr/bin/env bash

mkdir -p ../lib
clang -c -O2 -march=native -Os -fPIC -msse2 -msse -maes -I /usr/lib/clang/12.0.1/include cache.c
ar rcs ../lib/cache.a cache.o
