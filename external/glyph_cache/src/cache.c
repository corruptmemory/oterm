#include <stddef.h>
#include <stdint.h>
#include <xmmintrin.h>
#include <wmmintrin.h>
#include <string.h>
#include "cache.h"

#ifndef Assert
#define Assert(...)
#define GLYPH_TABLE_UNDEF_ASSERT
#endif

static void __movsb(unsigned char *, unsigned char const *, size_t);

#if defined(__i386__) || defined(__x86_64__)
static __inline__ void __movsb(unsigned char *__dst,
                               unsigned char const *__src,
                               size_t __n) {
  __asm__ __volatile__("rep movsb" : "+D"(__dst), "+S"(__src), "+c"(__n)
                       : : "memory");
}
#endif

#define DEBUG_VALIDATE_LRU 0

struct glyph_entry
{
    glyph_hash hash_value;

    uint32_t next_with_same_hash;
    uint32_t next_lru;
    uint32_t prev_lru;
    gpu_glyph_index gpu_index;

    // NOTE(casey): For user use:
    uint32_t filled_state;
    uint16_t dim_x;
    uint16_t dim_y;
    
#if DEBUG_VALIDATE_LRU
    size_t ordering;
#endif
};

struct glyph_table
{
    glyph_table_stats stats;

    uint32_t hash_mask;
    uint32_t hash_count;
    uint32_t entry_count;

    uint32_t *hash_table;
    glyph_entry *entries;

#if DEBUG_VALIDATE_LRU
    uint32_t last_lru_count;
#endif
};

static char unsigned overhang_mask[32] =
{
    255, 255, 255, 255,  255, 255, 255, 255,  255, 255, 255, 255,  255, 255, 255, 255,
    0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0
};

static char unsigned default_seed[16] =
{
    178, 201, 95, 240, 40, 41, 143, 216,
    2, 209, 178, 114, 232, 4, 176, 188
};

char unsigned *get_default_seed() {
    return default_seed;
}

glyph_hash compute_glyph_hash(size_t count, char unsigned *at, char unsigned *seedx16)
{
    /* TODO(casey):

      Consider and test some alternate hash designs.  The hash here
      was the simplest thing to type in, but it is not necessarily
      the best hash for the job.  It may be that less AES rounds
      would produce equivalently collision-free results for the
      problem space.  It may be that non-AES hashing would be
      better.  Some careful analysis would be nice.
    */

    // TODO(casey): Does the result of a grapheme composition
    // depend on whether or not it was RTL or LTR?  Or are there
    // no fonts that ever get used in both directions, so it doesn't
    // matter?

    // TODO(casey): Double-check exactly the pattern
    // we want to use for the hash here

    glyph_hash result = {0};

    // TODO(casey): Should there be an IV?
    __m128i hash_value = _mm_cvtsi64_si128(count);
    hash_value = _mm_xor_si128(hash_value, _mm_loadu_si128((__m128i *)seedx16));

    size_t ChunkCount = count / 16;
    while(ChunkCount--)
    {
        __m128i In = _mm_loadu_si128((__m128i *)at);

        hash_value = _mm_xor_si128(hash_value, In);
        hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());
        hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());
        hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());
        hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());
    }

    size_t overhang = count % 16;


#if 0
    __m128i In = _mm_loadu_si128((__m128i *)at);
#else
    // TODO(casey): This needs to be improved - it's too slow, and the #if 0 branch would be nice but can't
    // work because of overrun, etc.
    char temp[16];
    __movsb((unsigned char *)temp, at, overhang);
    __m128i In = _mm_loadu_si128((__m128i *)temp);
#endif
    In = _mm_and_si128(In, _mm_loadu_si128((__m128i *)(overhang_mask + 16 - overhang)));
    hash_value = _mm_xor_si128(hash_value, In);
    hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());
    hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());
    hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());
    hash_value = _mm_aesdec_si128(hash_value, _mm_setzero_si128());

    memcpy(result.value, &hash_value, sizeof(result.value));

    return result;
}

static gpu_glyph_index pack_glyph_cache_point(uint32_t x, uint32_t y)
{
    gpu_glyph_index result = {(y << 16) | x};
    return result;
}

glyph_cache_point unpack_glyph_cache_point(gpu_glyph_index p)
{
    glyph_cache_point result;

    result.x = (p.value & 0xffff);
    result.y = (p.value >> 16);

    return result;
}

static int glyph_hashes_are_equal(glyph_hash a, glyph_hash b)
{
    __m128i ab, bb;
    memcpy(&ab, a.value, sizeof(a.value));
    memcpy(&bb, b.value, sizeof(b.value));

    __m128i compare = _mm_cmpeq_epi32(ab, bb);
    int result = (_mm_movemask_epi8(compare) == 0xffff);

    return result;
}

static uint32_t *get_slot_pointer(glyph_table *table, glyph_hash run_hash)
{
    __m128i rhb;
    memcpy(&rhb, run_hash.value, sizeof(run_hash.value));
    uint32_t hash_index = _mm_cvtsi128_si32(rhb);
    uint32_t hash_slot = (hash_index & table->hash_mask);

    Assert(hash_slot < table->hash_count);
    uint32_t *result = &table->hash_table[hash_slot];

    return result;
}

static glyph_entry *get_entry(glyph_table *table, uint32_t index)
{
    Assert(index < table->entry_count);
    glyph_entry *result = table->entries + index;
    return result;
}

static glyph_entry *get_sentinel(glyph_table *table)
{
    glyph_entry *result = table->entries;
    return result;
}

glyph_table_stats get_and_clear_stats(glyph_table *table)
{
    glyph_table_stats result = table->stats;
    glyph_table_stats zero_stats = {0};
    table->stats = zero_stats;

    return result;
}

void update_glyph_cache_entry(glyph_table *table, uint32_t id, uint32_t new_state, uint16_t new_dim_x, uint16_t new_dim_y)
{
    glyph_entry *entry = get_entry(table, id);

    entry->filled_state = new_state;
    entry->dim_x = new_dim_x;
    entry->dim_y = new_dim_y;
}

#if DEBUG_VALIDATE_LRU
static void validate_lru(glyph_table *table, int expected_count_change)
{
    uint32_t entry_count = 0;

    glyph_entry *sentinel = get_sentinel(table);
    size_t last_ordering = sentinel->ordering;
    for(uint32_t entry_index = sentinel->next_lru;
        entry_index != 0;
        )
    {
        glyph_entry *entry = get_entry(table, entry_index);
        Assert(entry->ordering < last_ordering);
        last_ordering = entry->ordering;
        entry_index = entry->next_lru;
        ++entry_count;
    }

    if((table->last_lru_count + expected_count_change) != entry_count)
    {
        __debugbreak();
    }
    table->last_lru_count = entry_count;
}
#else
#define validate_lru(...)
#endif

static void recycle_lru(glyph_table *table)
{
    glyph_entry *sentinel = get_sentinel(table);

    // NOTE(casey): There are no more unused entries, evict the least recently used one
    Assert(sentinel->prev_lru);

    // NOTE(casey): Remove least recently used element from the LRU chain
    uint32_t entry_index = sentinel->prev_lru;
    glyph_entry *entry = get_entry(table, entry_index);
    glyph_entry *prev = get_entry(table, entry->prev_lru);
    prev->next_lru = 0;
    sentinel->prev_lru = entry->prev_lru;
    validate_lru(table, -1);

    // NOTE(casey): Find the location of this entry in its hash chain
    // NOTE(jim): Why the pointers here?
    uint32_t *next_index = get_slot_pointer(table, entry->hash_value);
    while(*next_index != entry_index)
    {
        Assert(*next_index);
        next_index = &get_entry(table, *next_index)->next_with_same_hash;
    }

    // NOTE(casey): Remove least recently used element from its hash chain, and place it on the free chain
    Assert(*next_index == entry_index);
    // NOTE(jim): This value isn't used
    *next_index = entry->next_with_same_hash;
    entry->next_with_same_hash = sentinel->next_with_same_hash;
    sentinel->next_with_same_hash = entry_index;

    // NOTE(casey): Clear the index count and state
    update_glyph_cache_entry(table, entry_index, 0, 0, 0);

    ++table->stats.recycle_count;
}

static uint32_t pop_free_entry(glyph_table *table)
{
    glyph_entry *sentinel = get_sentinel(table);

    if(!sentinel->next_with_same_hash)
    {
        recycle_lru(table);
    }

    uint32_t result = sentinel->next_with_same_hash;
    Assert(result);

    // NOTE(casey): Pop this unused entry off the sentinel's chain of unused entries
    glyph_entry *entry = get_entry(table, result);
    sentinel->next_with_same_hash = entry->next_with_same_hash;
    entry->next_with_same_hash = 0;

    Assert(entry);
    Assert(entry != sentinel);
    Assert(entry->dim_x == 0);
    Assert(entry->dim_y == 0);
    Assert(entry->filled_state == 0);
    Assert(entry->next_with_same_hash == 0);
    Assert(entry == get_entry(table, result));

    return result;
}

glyph_state find_glyph_entry_by_hash(glyph_table *table, glyph_hash run_hash)
{
    glyph_entry *result = 0;

    uint32_t *slot = get_slot_pointer(table, run_hash);
    uint32_t entry_index = *slot;
    while(entry_index)
    {
        glyph_entry *entry = get_entry(table, entry_index);
        if(glyph_hashes_are_equal(entry->hash_value, run_hash))
        {
            result = entry;
            break;
        }

        entry_index = entry->next_with_same_hash;
    }

    if(result)
    {
        Assert(entry_index);

        // NOTE(casey): An existing entry was found, remove it from the LRU
        glyph_entry *prev = get_entry(table, result->prev_lru);
        glyph_entry *Next = get_entry(table, result->next_lru);

        prev->next_lru = result->next_lru;
        Next->prev_lru = result->prev_lru;

        validate_lru(table, -1);

        ++table->stats.hit_count;
    }
    else
    {
        // NOTE(casey): No existing entry was found, allocate a new one and link it into the hash chain

        entry_index = pop_free_entry(table);
        Assert(entry_index);

        result = get_entry(table, entry_index);
        Assert(result->filled_state == 0);
        Assert(result->next_with_same_hash == 0);
        Assert(result->dim_x == 0);
        Assert(result->dim_y == 0);
        
        result->next_with_same_hash = *slot;
        result->hash_value = run_hash;
        *slot = entry_index;

        ++table->stats.miss_count;
    }

    // NOTE(casey): Update the LRU doubly-linked list to ensure this entry is now "first"
    glyph_entry *sentinel = get_sentinel(table);
    Assert(result != sentinel);
    result->next_lru = sentinel->next_lru;
    result->prev_lru = 0;

    glyph_entry *next_lru = get_entry(table, sentinel->next_lru);
    next_lru->prev_lru = entry_index;
    sentinel->next_lru = entry_index;

#if DEBUG_VALIDATE_LRU
    result->ordering = sentinel->ordering++;
#endif
    validate_lru(table, 1);

    glyph_state state;
    state.id = entry_index;
    state.dim_x = result->dim_x;
    state.dim_y = result->dim_y;
    state.gpu_index = result->gpu_index;
    state.filled_state = result->filled_state;

    return state;
}

void initialize_direct_glyph_table(glyph_table_params params, gpu_glyph_index *table, int skip_zero_slot)
{
    Assert(params.cache_tile_count_in_x >= 1);
    
    if(skip_zero_slot)
    {
        skip_zero_slot = 1;
    }
        
    uint32_t x = skip_zero_slot;
    uint32_t y = 0;
    for(uint32_t entry_index = 0;
        entry_index < (params.reserved_tile_count - skip_zero_slot);
        ++entry_index)
    {
        if(x >= params.cache_tile_count_in_x)
        {
            x = 0;
            ++y;
        }

        table[entry_index] = pack_glyph_cache_point(x, y);

        ++x;
    }
}

size_t get_glyph_table_footprint(glyph_table_params params)
{
    size_t hash_size = params.hash_count*sizeof(uint32_t);
    size_t entry_size = params.entry_count*sizeof(glyph_entry);
    size_t result = (sizeof(glyph_table) + hash_size + entry_size);

    return result;
}

glyph_table *place_glyph_table_in_memory(glyph_table_params params, void *memory)
{
    Assert(params.hash_count >= 1);
    Assert(params.entry_count >= 2);
    Assert(IsPowerOfTwo(params.hash_count));
    Assert(params.cache_tile_count_in_x >= 1);

    glyph_table *result = 0;

    if(memory)
    {
        // NOTE(casey): Always put the glyph_entry array at the base of the memory, because the
        // compiler may generate aligned-SSE ops, which would crash if it was unaligned.
        glyph_entry *entries = (glyph_entry *)memory;
        result = (glyph_table *)(entries + params.entry_count);
        result->hash_table = (uint32_t *)(result + 1);
        result->entries = entries;

        result->hash_mask = params.hash_count - 1;
        result->hash_count = params.hash_count;
        result->entry_count = params.entry_count;

        memset(result->hash_table, 0, result->hash_count*sizeof(result->hash_table[0]));

        uint32_t starting_tile = params.reserved_tile_count;

        glyph_entry *sentinel = get_sentinel(result);
        uint32_t x = starting_tile % params.cache_tile_count_in_x;
        uint32_t y = starting_tile / params.cache_tile_count_in_x;
        for(uint32_t entry_index = 0;
            entry_index < params.entry_count;
            ++entry_index)
        {
            if(x >= params.cache_tile_count_in_x)
            {
                x = 0;
                ++y;
            }

            glyph_entry *entry = get_entry(result, entry_index);
            if((entry_index+1) < params.entry_count)
            {
                entry->next_with_same_hash = entry_index + 1;
            }
            else
            {
                entry->next_with_same_hash = 0;
            }
            entry->gpu_index = pack_glyph_cache_point(x, y);

            entry->filled_state = 0;
            entry->dim_x = 0;
            entry->dim_y = 0;
            
            ++x;
        }

        get_and_clear_stats(result);
    }

    return result;
}

#ifdef  GLYPH_TABLE_UNDEF_ASSERT
#undef Assert
#endif
