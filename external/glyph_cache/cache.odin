package glyph_cache

import "core:os"
import "core:c"

when os.OS == "windows" do foreign import stbi "lib/cache.lib"
when os.OS == "linux" do foreign import glyph_cache "lib/cache.a"

Glyph_Table_Params :: struct {
  hash_count: u32,
  entry_count: u32,
  reserved_tile_count: u32,
  cache_tile_count_in_x: u32,
};

Gpu_Glyph_Index :: struct {
  value: u32,
};

Glyph_Cache_Point :: struct {
  x, y: u32,
};

Glyph_Hash_Proxy :: struct {
  value: [2]u64,
};

Glyph_State:: struct {
  id: u32,
  gpu_index: Gpu_Glyph_Index,
  filled_state: u32,
  dim_x: u16,
  dim_y: u16,
};

Glyph_Table_Stats :: struct {
    hit_count: c.size_t, // NOTE(casey): Number of times FindGlyphEntryByHash hit the cache
    miss_count: c.size_t, // NOTE(casey): Number of times FindGlyphEntryByHash misses the cache
    recycle_count: c.size_t,  // NOTE(casey): Number of times an entry had to be recycled to fill a cache miss
};


Glyph_Table :: struct {};

@(default_calling_convention="c")
foreign glyph_cache {
  get_default_seed :: proc() -> ^u8 ---;
  initialize_direct_glyph_table :: proc(params: Glyph_Table_Params, table: ^Gpu_Glyph_Index, skip_zero_slot: int) ---;
  get_glyph_table_footprint :: proc(params: Glyph_Table_Params) -> c.size_t ---;
  place_glyph_table_in_memory :: proc(params: Glyph_Table_Params, memory: rawptr) -> ^Glyph_Table ---;
  unpack_glyph_cache_point :: proc(p: Gpu_Glyph_Index) -> Glyph_Cache_Point ---;
  find_glyph_entry_by_hash :: proc(table: ^Glyph_Table, run_hash: Glyph_Hash_Proxy) -> Glyph_State ---;
  update_glyph_cache_entry :: proc(table: ^Glyph_Table, id: u32, new_state: u32, new_dim_x: u16, new_dim_y: u16) ---;
  get_and_clear_stats :: proc(table: ^Glyph_Table) -> Glyph_Table_Stats ---;
  compute_glyph_hash :: proc(count: uint, at: ^byte, seedx16: ^u8) -> Glyph_Hash_Proxy ---;
}
