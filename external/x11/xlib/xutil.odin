package xlib

VisualInfo :: struct {
  visual: ^Visual,
  visualid: VisualId,
  screen: i32,
  depth: i32,
  class: i32,
  red_mask: u64,
  green_mask: u64,
  blue_mask: u64,
  colormap_size: i32,
  bits_per_rgb: i32,
};