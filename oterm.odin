package main

import "core:log"
import "core:os"
import dl "core:dynlib"
import rt "core:runtime"
import "core:strings"
import "core:mem"
import gl "shared:odin-gl"
import x "shared:x11/xlib"
import ft "shared:freetype"
import lin "core:math/linalg"
import stb "shared:stb/stbi"
import glyph "./glyph"
import "core:hash"

Vector2i32 :: distinct [2]i32;

CharStruct :: struct {
  textureID: u32,            // ID handle of the glyph texture
  size: Vector2i32,          // Size of glyph
  bearing: Vector2i32,       // Offset from baseline to left/top of glyph
  texture_coord: Vector2i32, // Coordinate in the texture map
  advance: u32,              // Offset to advance to next glyph
}

ConstBuffer :: struct {
    cell_size: [2]u32,
    term_size: [2]u32,
    pad: u32,
};

TerminalCell :: struct {
    glyph_index: u32,
    foreground: u32,
    background: u32,
};

chars: [dynamic]CharStruct;
freetype: ft.Library;
glLib: dl.Library;
gluLib: dl.Library;
dpy: ^x.Display;
root: x.Window;
att := []i32{ gl.GLX_RGBA, gl.GLX_DEPTH_SIZE, 24, gl.GLX_DOUBLEBUFFER, gl.None };
vi: ^x.VisualInfo;
cmap: x.Colormap;
swa: x.XSetWindowAttributes;
win: x.Window;
glc: gl.GLXContext;
gwa: x.XWindowAttributes;
xev: x.XEvent;
max_height, max_width: u32;
texture_width, texture_height: u32;

attribs := []i32{
  gl.GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
  gl.GLX_CONTEXT_MINOR_VERSION_ARB, 6,
  0,
};

visual_attribs := []i32{
  gl.GLX_X_RENDERABLE    , gl.TRUE,
  gl.GLX_DRAWABLE_TYPE   , gl.GLX_WINDOW_BIT,
  gl.GLX_RENDER_TYPE     , gl.GLX_RGBA_BIT,
  gl.GLX_X_VISUAL_TYPE   , gl.GLX_TRUE_COLOR,
  gl.GLX_RED_SIZE        , 8,
  gl.GLX_GREEN_SIZE      , 8,
  gl.GLX_BLUE_SIZE       , 8,
  gl.GLX_ALPHA_SIZE      , 8,
  gl.GLX_DEPTH_SIZE      , 24,
  gl.GLX_STENCIL_SIZE    , 8,
  gl.GLX_DOUBLEBUFFER    , gl.TRUE,
  0,
};

draw_a_quad :: proc() {
 gl.ClearColor(1.0, 1.0, 1.0, 1.0);
 gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

 gl.MatrixMode(gl.PROJECTION);
 gl.LoadIdentity();
 gl.Ortho(-1., 1., -1., 1., 1., 20.);

 gl.MatrixMode(gl.MODELVIEW);
 gl.LoadIdentity();
 gl.uLookAt(0., 0., 10., 0., 0., 0., 0., 1., 0.);

 gl.Begin(gl.QUADS);
  gl.Color3f(1., 0., 0.); gl.Vertex3f(-.75, -.75, 0.);
  gl.Color3f(0., 1., 0.); gl.Vertex3f( .75, -.75, 0.);
  gl.Color3f(0., 0., 1.); gl.Vertex3f( .75,  .75, 0.);
  gl.Color3f(1., 1., 0.); gl.Vertex3f(-.75,  .75, 0.);
 gl.End();
}

fill_chars :: proc(face: ft.Face, chars_across: u32) {
  gl.PixelStorei(gl.UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction

  texture_rows := 128 / chars_across;
  if 128 % chars_across > 0 {
    texture_rows += 1;
  }

  // TODO(jim): This can be improved.  Look at using the BBox struct in the Face
  for c : u32 = 0; c < 128; c += 1 {
    if err := ft.Load_Char(face, c, ft.LOAD_RENDER); err != 0 {
      log.errorf("Failed to load Glyph: %v", err);
      os.exit(-1);
    }
    w := u32(face.glyph.metrics.width >> 6);
    if face.glyph.metrics.width % 64 > 0 {
      w += 1;
    }
    h := u32(face.glyph.metrics.height >> 6);
    if face.glyph.metrics.height % 64 > 0 {
      h += 1;
    }

    if w > max_width {
      max_width = w;
    }
    if h > max_height {
      max_height = h;
    }
  }

  origin := face.ascender >> 6;
  half_glyph := max_width/2;
  texture_width = max_width * chars_across;
  texture_height = max_height * texture_rows;
  glyph_len := max_width * max_height;
  bits := make([]byte, texture_width*texture_height);
  defer delete(bits);

  texture: u32;
  gl.GenTextures(1, &texture);
  x, y: u32;
  baseIdx: u32;
  for c : u32 = 0; c < 128; c += 1 {
    // load character glyph
    if err := ft.Load_Char(face, c, ft.LOAD_RENDER); err != 0 {
      log.errorf("Failed to load Glyph: %v", err);
      os.exit(-1);
    }
    glyph_bits := mem.slice_ptr(face.glyph.bitmap.buffer, int(glyph_len));
    row_end := face.glyph.bitmap.width;
    fixup := half_glyph - (face.glyph.bitmap.width/2);

    yfixup : i32 = i32(origin) - face.glyph.bitmap_top;
    for yi : i32 = 0; yi < i32(face.glyph.bitmap.rows); yi += 1 {
      ydest := yi + yfixup;
      ds := baseIdx + fixup + (u32(ydest) * texture_width);
      ss := u32(yi) * face.glyph.bitmap.width;
      copy(bits[ds:], glyph_bits[ss:row_end]);
      row_end += face.glyph.bitmap.width;
    }
    append(&chars, CharStruct{
      textureID = texture,
      size = Vector2i32{i32(max_width), i32(max_height)},
      bearing = Vector2i32{0, 0},
      texture_coord = Vector2i32{i32(x), i32(y)},
      advance = u32(face.glyph.advance.x) >> 6,
    });
    x += max_width;
    if x >= texture_width {
      x = 0;
      y += max_height;
    }
    baseIdx = x + (y * texture_width);
  }
  stb.write_png("./glyphs.png", int(texture_width), int(texture_height), 1, transmute([]u8)(bits), int(texture_width));
  log.debugf("texture: %d", texture);
  gl.BindTexture(gl.TEXTURE_RECTANGLE, texture);
  gl.TexImage2D(
    gl.TEXTURE_RECTANGLE,
    0,
    gl.RED,
    i32(texture_width),
    i32(texture_height),
    0,
    gl.RED,
    gl.UNSIGNED_BYTE,
    mem.raw_slice_data(bits),
  );
  gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.TexParameteri(gl.TEXTURE_RECTANGLE, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.BindTexture(gl.TEXTURE_RECTANGLE, 0);
}

render_text :: proc(s: u32, vao:u32, vbo: u32, text: string, x:f32, y:f32, scale: f32, color: lin.Vector3f32) {
    gl.UseProgram(s);
    gl.Uniform3f(gl.GetUniformLocation(s, "textColor"), color.x, color.y, color.z);
    gl.ActiveTexture(gl.TEXTURE0);
    gl.BindVertexArray(vao);

    lx := x;
    // iterate through all characters
    for c in text {
        ch := chars[c];

        xpos := lx + f32(ch.bearing.x) * scale;
        ypos := y - f32(ch.size.y - ch.bearing.y) * scale;

        w := f32(ch.size.x) * scale;
        h := f32(ch.size.y) * scale;
        // update VBO for each character
        vertices := [6][4]f32{
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 },
        };
        // render glyph texture over quad
        gl.BindTexture(gl.TEXTURE_2D, ch.textureID);
        // update content of VBO memory
        gl.BindBuffer(gl.ARRAY_BUFFER, vbo);
        gl.BufferSubData(gl.ARRAY_BUFFER, 0, size_of(f32) * 6 * 4, raw_slice_data(vertices[:]));
        gl.BindBuffer(gl.ARRAY_BUFFER, 0);
        // render quad
        gl.DrawArrays(gl.TRIANGLES, 0, 6);
        // now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        lx += f32(ch.advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
    }
    gl.BindVertexArray(0);
    gl.BindTexture(gl.TEXTURE_2D, 0);
}


render_cells :: proc(s: u32, vao:u32, vbo: u32, cells: []TerminalCell, screen_width: u32, screen_height: u32) {
  gl.UseProgram(s);
  gl.Enable(gl.BLEND);
  gl.ActiveTexture(gl.TEXTURE0);
  gl.BindVertexArray(vao);

  // iterate through all characters
  gl.BindTexture(gl.TEXTURE_RECTANGLE, chars[33].textureID);
  b := screen_height - (screen_height/max_height)*max_height;
  r := (screen_width/max_width)*max_width;
  top := f32(screen_height);
  right := f32(r);
  bottom := f32(b);

  vertices := [6][2]f32{
      { 0,     top    },
      { 0,     bottom },
      { right, bottom },

      { 0,     top    },
      { right, bottom },
      { right, top    },
  };
  gl.BindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.BufferSubData(gl.ARRAY_BUFFER, 0, size_of(f32) * 6 * 2, raw_slice_data(vertices[:]));
  gl.BindBuffer(gl.ARRAY_BUFFER, 0);
  // render quad
  gl.DrawArrays(gl.TRIANGLES, 0, 6);

  gl.BindVertexArray(0);
  gl.BindTexture(gl.TEXTURE_RECTANGLE, 0);
}


load_sym :: proc(p: rawptr, name: cstring) {
  // log.debugf("Loading: %s", string(name));
  n := string(name);
  rp: rawptr;
  found: bool;
  if strings.has_prefix(n, "glu") {
    rp, found = dl.symbol_address(gluLib, n);
  } else {
    rp, found = dl.symbol_address(glLib, n);
  }

  if !found {
    log.errorf("error looking up: %s", n);
  }
  (cast(^rawptr)p)^ = rp;
}

debug_log :: proc(type_: u32, severity: u32, message: string) {
  log.errorf("type = 0x%x, severity = 0x%x, message = %s", type_, severity, message);
}

debug_log_callback :: proc "c" (source: u32,
                                type_: u32,
                                id: u32,
                                severity: u32,
                                length: i32,
                                message: ^byte,
                                userParam: rawptr) {
  context = rt.default_context();
  log.info("here");
  debug_log(type_ = type_, severity = severity, message = strings.string_from_ptr(message, int(length)));
}

check_error :: proc(message: string) {
  err := gl.GetError();
  if err != gl.NO_ERROR {
    log.errorf("OpenGL error: %v", err);
    log.error(message);
    os.exit(-1);
  }
}

fill_cells :: proc(cells_ssbo: u32, cells: ^[]TerminalCell, screen_width: u32, screen_height: u32, max_width: u32, max_height: u32) {
  cells_across := screen_width / max_width;
  cells_down := screen_height / max_height;

  cell_count := cells_across * cells_down;

  if cells^ != nil {
    delete(cells^);
  }

  cells^ = make([]TerminalCell, cell_count);

  glyphs := 126 - 33;
  for _, i in cells^ {
    ch := chars[(i % glyphs) + 33];
    cells^[i] = TerminalCell {
      glyph_index = u32((ch.texture_coord.y << 16) | ch.texture_coord.x),
      foreground = 0x00FFFFFF,
      background = 0x00000000,
    };
  }
  gl.BindBuffer(gl.SHADER_STORAGE_BUFFER, cells_ssbo);
  gl.BufferData(gl.SHADER_STORAGE_BUFFER, size_of(TerminalCell) * int(cell_count), mem.raw_slice_data(cells^), gl.DYNAMIC_DRAW);
  gl.BindBufferBase(gl.SHADER_STORAGE_BUFFER, 2, cells_ssbo);
  gl.BindBuffer(gl.SHADER_STORAGE_BUFFER, 0);
}

main :: proc() {
  logger := log.create_console_logger(lowest = log.Level.Debug);
  context.logger = logger;
  loaded: bool;

  gcache_config := glyph.Glyph_Table_Params {
    hash_count = 4096,
    entry_count = 896,
    reserved_tile_count = 128,
    cache_tile_count_in_x = 64,
  };

  footprint := glyph.get_glyph_table_footprint(gcache_config);

  log.debugf("footprint: %d", footprint);

  xxx := "a";
  r := hash.adler32(transmute([]byte)xxx);

  log.debugf("r: %v", r);

  glLib, loaded = dl.load_library("/usr/lib/libGL.so");
  if !loaded {
    log.error("Failed to load OpenGL");
    os.exit(-1);
  }
  log.debug("Woot! Loaded OpenGL");
  defer dl.unload_library(glLib);

  gluLib, loaded = dl.load_library("/usr/lib/libGLU.so");
  if !loaded {
    log.error("Failed to load OpenGL Utility lib");
    os.exit(-1);
  }
  log.debug("Woot! Loaded OpenGL Utility");
  defer dl.unload_library(gluLib);

  if err := ft.Init_FreeType(&freetype); err != 0 {
    log.error("Failed to initialize Freetype");
    os.exit(-1);
  }
  defer ft.Done_FreeType(freetype);

  face: ft.Face;
  if err := ft.New_Face(freetype, "/usr/share/fonts/source-code-pro/SourceCodePro-Regular.ttf", 0, &face); err != 0 {
    log.error("Failed to load face");
    os.exit(-1);
  }
  defer ft.Done_Face(face);

  log.debugf("Face: %v", face);

  charmaps := mem.slice_ptr(face.charmaps, int(face.num_charmaps));

  log.debugf("Charmaps: %v", charmaps);

  if err := ft.Set_Char_Size(face, 0, 16*64, 92, 92); err != 0 {
    log.error("Failed to set face size");
    os.exit(-1);
  }

  gl.load_up_to(4, 6, load_sym);

  dpy = x.open_display(nil);

  if dpy == nil {
    log.error("Cannot connect to X server");
    os.exit(-1);
  }

  fbcount: i32;
  fbc := gl.XChooseFBConfig(dpy, x.default_screen(dpy), raw_slice_data(visual_attribs), &fbcount);
  if fbc == nil {
    log.error("Error in XChooseFBConfig");
    os.exit(-1);
  }
  log.infof("Found %d fbconfigs", fbcount);
  fbcs := mem.slice_ptr(fbc, int(fbcount));

  best_fbc : i32 = -1;
  best_num_samp : i32 = -1;

  for fb, i in fbcs {
    vi1 := gl.XGetVisualFromFBConfig(dpy, fb);
    if vi1 != nil {
      samp_buf, samples: i32;
      gl.XGetFBConfigAttrib(dpy, fb, gl.GLX_SAMPLE_BUFFERS, &samp_buf);
      gl.XGetFBConfigAttrib(dpy, fb, gl.GLX_SAMPLES       , &samples);

      if best_fbc < 0 || (samp_buf != 0 && samples > best_num_samp) {
        best_fbc = i32(i);
        best_num_samp = samples;
      }
    }
    x.free(vi1);
  }

  bestFbc := fbcs[best_fbc];

  // x.free(fbc);

  root = x.default_root_window(dpy);
  log.debugf("root: %v", root);
  vi = gl.XGetVisualFromFBConfig(dpy, bestFbc);

  if vi == nil {
    log.error("No appropriate visual found");
    os.exit(-1);
  }
  log.infof("vi: %v", vi);
  log.infof("Visual %x selected", vi.visualid);
  log.infof("Depth %d selected", vi.depth);

  cmap = x.create_colormap(dpy, root, vi.visual, x.AllocNone);

  swa.colormap = cmap;
  swa.eventMask = x.ExposureMask | x.KeyPressMask;

  win = x.create_window(dpy, root, 0, 0, 600, 600, 0, vi.depth, x.InputOutput, vi.visual, x.CWColormap | x.CWEventMask, &swa);

  x.map_window(dpy, win);
  x.store_name(dpy, win, "VERY SIMPLE APPLICATION");

  glc = gl.XCreateContextAttribsARB(dpy, bestFbc, nil, gl.TRUE, (^i32)(raw_slice_data(attribs)));
  gl.XMakeCurrent(dpy, win, glc);

  // gl.Enable(gl.DEPTH_TEST);
  // gl.Enable(gl.CULL_FACE);
  gl.Enable(gl.BLEND);
  // gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
  gl.BlendFunc(gl.SRC_ALPHA, gl.DST_ALPHA);
  gl.Enable(gl.DEBUG_OUTPUT);
  gl.DebugMessageCallback(debug_log_callback, &logger);

  program, ok := gl.load_shaders_file("shaders/oterm.vert", "shaders/oterm.frag");
  if !ok {
    log.error("Error loading shaders");
    os.exit(-1);
  }
  defer gl.DeleteProgram(program);

  gl.UseProgram(program);

  vbo, vao: u32;
  gl.GenVertexArrays(1, &vao);
  gl.GenBuffers(1, &vbo);
  gl.BindVertexArray(vao);
  gl.BindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.BufferData(gl.ARRAY_BUFFER, size_of(f32) * 6 * 2, nil, gl.DYNAMIC_DRAW);
  gl.EnableVertexAttribArray(0);
  gl.VertexAttribPointer(0, 2, gl.FLOAT, gl.FALSE, 2 * size_of(f32), nil);
  gl.BindBuffer(gl.ARRAY_BUFFER, 0);
  gl.BindVertexArray(0);
  // gl.LoadIdentity();
  // gl.Ortho(0, 600, 0, 600, -1, 1);

  projection:lin.Matrix4x4f32 = lin.matrix_ortho3d_f32(0, 600, 0, 600, -1, 1);
  pp : ^f32 = &projection[0][0];
  gl.UniformMatrix4fv(gl.GetUniformLocation(program, "projection"), 1, gl.FALSE, pp);

  // gl.ActiveTexture(gl.TEXTURE0);
  fill_chars(face, 64);
  gl.Uniform1i(gl.GetUniformLocation(program, "glyphTexture"), 0);

  terminal_dims := ConstBuffer {
    cell_size = [2]u32{max_width, max_height},
    term_size = [2]u32{600/max_width, 600/max_height},
  };

  const_buffer := gl.GetUniformBlockIndex(program, "constBuffer");
  log.debugf("const_buffer: %v", const_buffer);
  const_buffer_uniform: u32;
  gl.UniformBlockBinding(program, const_buffer, 0);
  gl.GenBuffers(1, &const_buffer_uniform);
  gl.BindBuffer(gl.UNIFORM_BUFFER, const_buffer_uniform);
  gl.BufferData(gl.UNIFORM_BUFFER, size_of(ConstBuffer), &terminal_dims, gl.DYNAMIC_DRAW);
  gl.BindBufferBase(gl.UNIFORM_BUFFER, 0, const_buffer_uniform);
  gl.BindBuffer(gl.UNIFORM_BUFFER, 0);

  cells_ssbo: u32;
  gl.GenBuffers(1, &cells_ssbo);
  cells: []TerminalCell;
  // fill_cells(cells_ssbo, &cells, 600, 600, max_width, max_height);

  for {
    x.next_event(dpy, &xev);

    if xev.type == x.Expose {
      x.get_window_attributes(dpy, win, &gwa);
      gl.Viewport(0, 0, gwa.width, gwa.height);
      // gl.Viewport(0, 0, 1, 1);
      // gl.LoadIdentity();
      // gl.Ortho(0, f64(gwa.width), 0, f64(gwa.height), -1, 1);
      projection = lin.matrix_ortho3d_f32(0, f32(gwa.width), 0, f32(gwa.height), 0, 1);
      pp = &projection[0][0];
      gl.UniformMatrix4fv(gl.GetUniformLocation(program, "projection"), 1, gl.FALSE, pp);
      terminal_dims.term_size = [2]u32{u32(gwa.width/i32(max_width)), u32(gwa.height/i32(max_height))};

      const_buffer = gl.GetUniformBlockIndex(program, "constBuffer");
      gl.UniformBlockBinding(program, const_buffer, 0);
      gl.GenBuffers(1, &const_buffer_uniform);
      gl.BindBuffer(gl.UNIFORM_BUFFER, const_buffer_uniform);
      gl.BufferData(gl.UNIFORM_BUFFER, size_of(ConstBuffer), &terminal_dims, gl.DYNAMIC_DRAW);
      gl.BindBufferBase(gl.UNIFORM_BUFFER, 0, const_buffer_uniform);
      gl.BindBuffer(gl.UNIFORM_BUFFER, 0);

      gl.ClearColor(0.2, 0.3, 0.3, 1.0);
      gl.Clear(gl.COLOR_BUFFER_BIT);

      fill_cells(cells_ssbo, &cells, u32(gwa.width), u32(gwa.height), max_width, max_height);
      render_cells(program, vao, vbo, cells, u32(gwa.width), u32(gwa.height));

      gl.XSwapBuffers(dpy, win);
    }
    //  else if xev.type == x.KeyPress {
    //   gl.XMakeCurrent(dpy, gl.None, nil);
    //   gl.XDestroyContext(dpy, glc);
    //   x.destroy_window(dpy, win);
    //   x.close_display(dpy);
    //   os.exit(0);
    // }
  }

}
