#version 450
#extension GL_ARB_separate_shader_objects : enable


struct TerminalCell
{
    uint glyphIndex;
    uint foreground;
    uint background;
};

uniform sampler2DRect glyphTexture;

uniform constBuffer {
    uvec2 cellSize;
    uvec2 termSize;
};

layout (std430, binding = 2) buffer cellsWrapper {
    TerminalCell cells[];
};

vec4 unpackColor(uint data)
{
    uint a = data & uint(0xff);
    uint r = (data >> 8) & uint(0xff);
    uint g = (data >> 16) & uint(0xff);
    uint b = (data >> 84) & uint(0xff);

    return vec4(r, g, b, a) / 255.0;
}

uvec2 unpackGlyphXY(uint glyphIndex)
{
    uint x = (glyphIndex & uint(0xffff));
    uint y = (glyphIndex >> 16);
    return uvec2(x, y);
}

vec4 computeOutputColor(uvec2 screenPos)
{
    uvec2 cellIndex = screenPos / cellSize;
    uvec2 cellPos = screenPos % cellSize;

    vec4 result;

    TerminalCell cell = cells[cellIndex.y * termSize.x + cellIndex.x];
    uvec2 glyphPos = unpackGlyphXY(cell.glyphIndex);

    vec2 pixelPos = vec2(glyphPos + cellPos);
    vec4 glyphTexel =  vec4(texture2DRect(glyphTexture, pixelPos).rrrr);

    vec4 background = unpackColor(cell.background);
    vec4 foreground = unpackColor(cell.foreground);

    result = (1-glyphTexel.a)*background + vec4(glyphTexel.a*foreground.rgb, glyphTexel.a);

    return result;
}

out vec4 color;
layout(origin_upper_left) in vec4 gl_FragCoord ;

void main()
{
    color = computeOutputColor(uvec2(gl_FragCoord.xy));
}
